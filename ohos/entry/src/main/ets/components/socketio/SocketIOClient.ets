import { client_socket } from '@ohos/socketio';
import { DeviceType } from './DeviceType';
import { SocketOptions } from './SocketOptions';

export default class SocketIOClient {

  //client
  client: client_socket = new client_socket();
  //连接选项
  socketOptions: SocketOptions = new SocketOptions();
  //连接状态
  isConnected: boolean = false;
  //终端类型
  deviceType: DeviceType = DeviceType.DEFAULT;

  /**
   * 准备连接
   * @param deviceType
   * @param socketOptions
   */
  prepare(deviceType: DeviceType, socketOptions: SocketOptions): void {
    this.deviceType = deviceType;
    this.socketOptions = socketOptions;
  }

  /**
   * 连接
   */
  connect(): void {
    if(this.socketOptions.uri === "") {
      return;
    }
    this.client.set_open_listener(() => {
      this.isConnected = true;
      console.log("SOCKET_IO on_open");
    });
    this.client.set_fail_listener(() => {
      console.log("SOCKET_IO on_fail");
    });
    this.client.set_reconnecting_listener(() => {
      console.log("SOCKET_IO on_reconnecting");
    });
    this.client.set_reconnect_listener(() => {
      console.log("SOCKET_IO on_reconnect");
    });
    this.client.set_close_listener((reason: string) => {
      this.isConnected = false;
      console.log("SOCKET_IO on_close: " + reason);
    });
    this.client.set_socket_open_listener((nsp: string) => {
      console.log("SOCKET_IO on_socket_open: " + nsp);
    });
    this.client.set_socket_close_listener((nsp: string) => {
      console.log("SOCKET_IO on_socket_close: " + nsp);
    });
    this.client.connect(this.socketOptions.uri);
    this.client.set_reconnect_attempts(this.socketOptions.reconnectAttempts);
    this.client.set_reconnect_delay(this.socketOptions.reconnectDelay);
    this.client.set_reconnect_delay_max(this.socketOptions.reconnectDelayMax);
  }

  /**
   * 判断是否连接
   * @returns
   */
  isConnect(): boolean {
    return this.isConnected;
  }

  /**
   * 关闭连接
   */
  disconnect(): void {
    this.client.close();
  }

  /**
   * 发送消息
   * @param event 事件
   * @param message 消息内容
   */
  sendMessage(event: string, message: string) {
    if (!this.isConnected) {
      return;
    }
    this.client.emit(event, message);
  }

}
