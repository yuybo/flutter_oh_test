import { BinaryMessenger, MethodCall } from '@ohos/flutter_ohos';
import MethodChannel, {
  MethodCallHandler,
  MethodResult
} from '@ohos/flutter_ohos/src/main/ets/plugin/common/MethodChannel';
import Share from './Share';
import { buffer } from '@kit.ArkTS';
import { common } from '@kit.AbilityKit';

//方法通道名称
const METHOD_CHANNEL_NAME = "hnair/share";

export default class SharePluginMethodCallHandlerImpl implements MethodCallHandler {
  private share: Share | null = null;
  private channel: MethodChannel | null = null;

  constructor(share: Share) {
    this.share = share;
  }

  /**
   * 开启通道监听
   * @param messenger
   */
  startListening(messenger: BinaryMessenger) {
    if (this.channel != null) {
      this.stopListening();
    }
    this.channel = new MethodChannel(messenger, METHOD_CHANNEL_NAME);
    this.channel.setMethodCallHandler(this);
  }

  /**
   * 关闭通道监听
   */
  stopListening() {
    if (this.channel == null) {
      return;
    }
    this.channel.setMethodCallHandler(null);
    this.channel = null;
  }

  onMethodCall(call: MethodCall, result: MethodResult): void {
    let method: string = call.method;
    switch (method) {
      case 'share':
        this.handleShare(call, result);
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  /**
   * 分享
   * @param call
   * @param result
   */
  handleShare(call: MethodCall, result: MethodResult): void {
    const uiAbilityContext: common.UIAbilityContext = getContext(this) as common.UIAbilityContext;
    let type: string = call.argument("type");
    if ("text" === type) {
      let text: string = call.argument('text');
      this.share?.shareText(text, uiAbilityContext, result);
    } else {
      let buf: buffer.Buffer = buffer.from(call.argument("imageData"));
      this.share?.shareImage(buf, uiAbilityContext, result);
    }
  }

}