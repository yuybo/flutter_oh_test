import { AbilityPluginBinding, FlutterPlugin, FlutterPluginBinding } from "@ohos/flutter_ohos";
import { FaceDetectHandler } from "./FaceDetectHandler";
import { FaceDetector } from "./FaceDetector";

export default class FaceDetectPlugin implements FlutterPlugin {

  private abilityBinding: AbilityPluginBinding | null = null;
  private methodCallHandler: FaceDetectHandler | null = null;

  onAttachedToAbility(binding: AbilityPluginBinding): void {
    this.abilityBinding = binding;
  }

  onDetachedFromAbility(): void {

  }

  getUniqueClassName(): string {
    return "FaceDetectPlugin";
  }

  onAttachedToEngine(binding: FlutterPluginBinding): void {
    this.methodCallHandler = new FaceDetectHandler(new FaceDetector());
    this.methodCallHandler.startListening(binding.getBinaryMessenger());
  }

  onDetachedFromEngine(binding: FlutterPluginBinding): void {
    if (this.methodCallHandler != null) {
      this.methodCallHandler.stopListening();
      this.methodCallHandler = null;
    }
  }
}