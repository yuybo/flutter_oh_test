import {
  bundleManager,
  common,
  abilityAccessCtrl,
  PermissionRequestResult,
} from '@kit.AbilityKit';

const TAG = 'Scan SampleCode PermissionsUtil';
let context = getContext(this) as common.UIAbilityContext;

export class PermissionsUtil {

  /**
   * 验证权限授权状态
   * @param permission
   * @returns
   */
  public static async hasCameraPermission(): Promise<abilityAccessCtrl.GrantStatus> {
    let atManager = abilityAccessCtrl.createAtManager();
    let grantStatus: abilityAccessCtrl.GrantStatus = abilityAccessCtrl.GrantStatus.PERMISSION_DENIED;
    let tokenId: number = 0;
    try {
      let bundleInfo: bundleManager.BundleInfo =
        await bundleManager.getBundleInfoForSelf(bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION);
      let appInfo: bundleManager.ApplicationInfo = bundleInfo.appInfo;
      tokenId = appInfo.accessTokenId;
    } catch (error) {
      console.error(TAG, `Failed to get bundle info for self. Code: ${error.code}.`);
    }
    try {
      grantStatus = await atManager.checkAccessToken(tokenId, 'ohos.permission.CAMERA');
    } catch (error) {
      console.error(TAG, `Failed to check access token. Code: ${error.code}.`);
    }
    return grantStatus;
  }

  /**
   * 申请授权
   * @returns
   */
  public static async reqPermissionsFromUser(): Promise<number[]> {
    let atManager = abilityAccessCtrl.createAtManager();
    let grantStatus: PermissionRequestResult = { permissions: [], authResults: [] }
    try {
      grantStatus = await atManager.requestPermissionsFromUser(context, ['ohos.permission.CAMERA']);
    } catch (error) {
      console.error(TAG, `Failed to request permissions from user. Code: ${error.code}.`);
    }
    return grantStatus.authResults;
  }
}