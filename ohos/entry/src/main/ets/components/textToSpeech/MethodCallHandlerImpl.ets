import { BinaryMessenger, MethodCall, MethodCallHandler, MethodChannel, MethodResult } from "@ohos/flutter_ohos";
import { SystemTTS } from "./SystemTTS";

//方法通道名称
const METHOD_CHANNEL_NAME = "com.t3go/text_to_speech";

export default class MethodCallHandlerImpl implements MethodCallHandler {
  private tts: SystemTTS | null = null;
  private channel: MethodChannel | null = null;

  /**
   * 初始化SystemTTS对象实例
   * @param tts
   */
  initTTS(tts: SystemTTS | null) {
    this.tts = tts;
    this.tts?.initTTSEngine();
  }

  /**
   * 开启通道监听
   * @param messenger
   */
  startListening(messenger: BinaryMessenger) {
    if (this.channel != null) {
      this.stopListening();
    }
    this.channel = new MethodChannel(messenger, METHOD_CHANNEL_NAME);
    this.channel.setMethodCallHandler(this);
  }

  /**
   * 关闭通道监听
   */
  stopListening() {
    if (this.channel == null) {
      return;
    }
    this.channel.setMethodCallHandler(null);
    this.channel = null;
  }

  /**
   * 通道方法处理
   * @param call
   * @param result
   */
  onMethodCall(call: MethodCall, result: MethodResult): void {
    const method = call.method;
    switch (method) {
      //是否播报中
      case "isSpeaking":
        result.success(this.tts?.isSpeaking());
        break;
      //播报
      case "speak":
        let text: string = call.argument("text") ?? "";
        this.tts?.speak(text);
        result.success(true);
        break;
      //停止播报
      case "stop":
        this.tts?.stop();
        result.success(true);
        break;
      //设置播报音量
      case "setVolume":
        let volume: number = call.argument("volume");
        if (volume != null) {
          result.success(this.tts?.setVolume(volume));
        } else {
          result.success(false)
        }
        break;
      //设置播报音量倍数
      case "setPitch":
        let pitch: number = call.argument("pitch");
        if (pitch != null) {
          result.success(this.tts?.setPitch(pitch));
        } else {
          result.success(false)
        }
        break;
      //设置播报速率
      case "setRate":
        let rate: number = call.argument("rate");
        if (rate != null) {
          result.success(this.tts?.setRate(rate));
        } else {
          result.success(false)
        }
        break;
      //设置播报语言，当前只支持中文播报zh-CN
      case "setLanguage":
        let language: string = call.argument("language");
        if (language != null) {
          result.success(this.tts?.setLanguage(language));
        } else {
          result.success(false)
        }
        break;
      //获取默认播报语言
      case "getDefaultLanguage":
        result.success(this.tts?.getDefaultLanguage());
        break;
      //获取所有支持的播报语言
      case "getLanguages":
        result.success(this.tts?.getAvailableLanguages());
        break;
      //获取音色相关信息
      case "getVoice":
        result.success(this.tts?.getVoices());
        break;
      default:
        result.notImplemented();
        break;
    }
  }
}