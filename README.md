## Flutter侧Dart与Html侧JS通信

### 背景

当前三方框架flutter_webview不支持prompt特性，影响dart与js侧之间的通信，该问题影响伙伴70%的业务功能开发。

> 说明：在 Flutter WebView 中，"prompt" 特性是指 WebView 和 Flutter 之间进行通信的一种方式。在 Web 页面中，可以使用 JavaScript 的 prompt 函数来触发 Flutter 代码的执行。

### 方案


dart不直接与js通信，而是通过arkts进行中转后，间接实现通信效果。

### 前置环境及项目构建
拉取flutter_flutter项目，https://gitee.com/openharmony-sig/flutter_flutter，注意git切换到dev分支
根据指引配置环境变量，下载flutter_engine构建的产物，并使用flutter命令创建flutter ohos版的项目
已经构建好的产物可以直接下载使用，下载地址：https://docs.qq.com/sheet/DUnljRVBYUWZKZEtF?tab=BB08J2
将下载的构建产物在本地某个文件夹解压，如E:\workspace\demo\flutter_engine，后面构建和运行项目的时候要用

//构建hap包
flutter build hap --local-engine="E:\workspace\demo\flutter_engine\src\out\ohos_debug_unopt_arm64"

//运行项目  -d 022789239B155256 这个是鸿蒙真机设备id
flutter run --debug --local-engine="E:\workspace\demo\flutter_engine\src\out\ohos_debug_unopt_arm64" -d 022789239B155256


#### Dart与ArkTs桥接

* **Dart侧**  

在lib下的main.dart

1. 引入services.dart
import 'package:flutter/services.dart';

2. 创建MethodChannel对象。

// channelName自己定义字符串，跟ArkTs一致就行，相当于双方约定通道名称，后续在这个通道上进行通信
final MethodChannel _channel = MethodChannel('channelName');


2. 定义`authenticate`原生方法与ArkTs通信,方法名称自定义。

```dart
Future<void> authenticate(dynamic arguments) async {
  print('xxx调用authenticate方法');
  String result = (await _channel.invokeMethod<String>('authenticate', arguments)) ?? "xxx";
  setState(() {
    _counter = result;
  });
  print('result=$result');
}

//authenticate是异步方法，所以在调用他的位置加await让他同步执行 
void _incrementCounter() async {
	  print('============xxx调用authenticate方法前============');
	  await authenticate("11fff");
	  print('============xxx调用authenticate方法后============');
  }
```
invokeMethod方法表示反射调用方法，参数1表示具体的方法名称，参数2是传输的参数，此方法返回的是Promise对象，所以加上await表示同步等待方法执行结束，返回结果值。



* **ArkTs侧** TestPlugin.ets

1. 定义通道名称，跟前面dart那边约定一致

const CHANNEL_NAME = "plugins.flutter.io/test_ohos";

2. 定义TestPlugin插件类，实现FlutterPlugin接口

定义MethodChannel变量
private channel: MethodChannel | null = null;

3. 复写FlutterPlugin接口中的onAttachedToEngine方法

```javascript
onAttachedToEngine(binding: FlutterPluginBinding): void {
    Log.d(TAG, 'onAttachedToEngine local auth')
    //创建`MethodChannel`对象
    this.channel = new MethodChannel(binding.getBinaryMessenger(), CHANNEL_NAME);
    this.channel.setMethodCallHandler({
      onMethodCall(call: MethodCall, result: MethodResult): void {
        Log.d(TAG, 'onMethodCall step in')
        //AppStorage.setOrCreate("sendArgs", call.args)
        let eventData: Record<string, string> = {
          "data": call.args
        };
        //emitter监听receiveData事件
        emitter.on("receiveData", (eventData) => {
          Log.d("xxxeventData: ", JSON.stringify(eventData.data))
          //给dart侧返回结果
          result.success(JSON.stringify(eventData.data));
        });
        //emitter向web侧发送sendData事件，发送dart传递过来的参数
        emitter.emit("sendData", eventData)
        //监听方法名称，实现对应的业务逻辑
        switch (call.method) {
          case "authenticate":
            Log.d("dart args: ", call.args)
            //给dart侧返回结果
            result.success(11);
            break;
          default:
            //result.success(1);
            break;
        }
      }
    });
  }
```

4. 在EntryAbility中引入并添加插件类
```javascript
  import TestPlugin from '../plugins/TestPlugin'
  //注册插件
  this.addPlugin(new TestPlugin())
```    



#### ArkTs与JS桥接

* **ArkTs侧** 

```javascript
controller: web_webview.WebviewController = new web_webview.WebviewController();
ports: web_webview.WebMessagePort[] = [];

//监听arkTs侧发送数据事件
aboutToAppear(): void {
    emitter.on("sendData", (eventData) => {
      console.info("xxxSendData:", eventData.data)
      this.ports[1].postMessageEvent('我是dart的数据' + eventData.data);
    });
  }

Web()
  .onPageEnd(() => {
    // 1.创建两个消息端口。
    this.ports = this.controller.createWebMessagePorts();
    
    // 2.在ArkTs侧端口(端口1)注册监听事件
    this.ports[1].onMessageEvent((result: web_webview.WebMessage) => {
      let eventData: Record<string, string> = {
        "data": result as string
      };
      console.info("xxxBackData:", eventData.data)
      emitter.emit("receiveData", eventData)
    })
    
    // 3.发送端口0到HTML侧
    this.controller.postMessage('__init_port__', [this.ports[0]], '*');
  })

// 通过端口1发送消息到HTML侧
Button('sendData').onClick((event: ClickEvent) => {
    this.ports[1].postMessageEvent('我是ets的数据');
})
```

* **HTML侧**

```javascript
<script>
window.addEventListener('message', function (event) {
  if (event.data === '__init_port__') {
    if (event.ports[0] !== null) {
      // 1. 保存从应用侧发送过来的端口
      let h5Port = event.ports[0];
      h5Port.onmessage = function (event) {
        // 2. 接收ets侧发送过来的消息。
        let result = event.data; // 从ArkTs获取到的数据
        let toEtsResult = result + '我是h5返回的消息';
        // 3. 使用h5Port向应用侧发送消息。
        h5Port.postMessage(data);
      }
    }
  }
})
</script>
```

