import 'package:flutter/material.dart';

import './models/AirOrderInfo.dart';
import 'widgets/LeftLineWidget.dart';
import 'widgets/fee_detail.dart';
import 'widgets/select_passenger.dart';

class WidgetDemo extends StatefulWidget {
  const WidgetDemo({super.key});

  @override
  State<WidgetDemo> createState() => _WidgetDemoState();
}

class _WidgetDemoState extends State<WidgetDemo> {
  final AirOrderInfo airOrderInfo = AirOrderInfo(
    ticketAmount: 800.00,
    airFund: 50.00,
    fuelAmount: 70.00,
    personNum: 1,
    childNum: 0,
    babyNum: 0,
  );

  ///显示费用详情
  void showFeeDetail() {
    showModalBottomSheet(
      context: context,
      //isScrollControlled: true,
      isDismissible: false,
      builder: (BuildContext context) {
        return Container(
          height: 600,
          color: Colors.amber,
          child: Center(
            child: FeeDetail(airOrderInfo: airOrderInfo),
          ),
        );
      },
    );
  }

  ///显示乘机人
  void showPassengerModel() {
    showModalBottomSheet(
      context: context,
      isDismissible: false,
      builder: (BuildContext context) {
        return Container(
          height: 600,
          color: Colors.amber,
          child: const Center(
            child: SelectPassenger(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("WidgetDemo"),
      ),
      body: Center(
          child: Column(children: <Widget>[
        buildJourneyItemWidget("2024-05-11", "16:00", "海口 美兰机场 T3航站楼",
            const Icon(Icons.local_airport)),
        buildJourneyItemDetailWidget(),
        buildJourneyItemWidget("2024-05-11", "16:00", "北京 大兴国际机场 T2航站楼",
            const Icon(Icons.local_airport_rounded)),
        buildPassengerItem()
      ])),
      floatingActionButton: FloatingActionButton(
        onPressed: showFeeDetail,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }

  ///画线与图标
  Widget buildIconWidget(Icon icon) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(
            width: 30,
            height: 15,
            child: LeftLineWidget(true, true, true, 0, 15),
          ),
          Container(
            width: 30,
            height: 30,
            margin: const EdgeInsets.only(right: 10),
            child: icon,
          ),
          const SizedBox(
            width: 30,
            height: 15,
            child: LeftLineWidget(true, true, true, 0, 15),
          ),
        ]);
  }

  ///构建行程详情
  Widget buildJourneyItemDetailWidget() {
    return Container(
        height: 150,
        color: Colors.white,
        child: Row(children: <Widget>[
          const SizedBox(
            width: 30,
            height: 150,
            child: LeftLineWidget(true, true, true, 0, 150),
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[Text("HU7081 海南航空"), Text("总时长：3小时45分")]),
        ]));
  }

  /// 构建行程列表项
  Widget buildJourneyItemWidget(
      String date, String time, String air, Icon icon) {
    return Container(
        margin: const EdgeInsets.only(top: 10),
        height: 60,
        color: Colors.grey[300],
        child: Row(children: <Widget>[
          buildIconWidget(icon),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[Text(date), Text(time)]),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[Text(air)])
        ]));
  }

  ///构建乘机人
  Widget buildPassengerItem() {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.only(left: 10, right: 10),
      height: 60,
      color: Colors.grey[300],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          const Text("乘机人", style: TextStyle(fontSize: 14, height: 2.0)),
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.red)),
            onPressed: () {
              showPassengerModel();
            },
            child: const Text("选择乘机人"),
          )
        ],
      ),
    );
  }
}
