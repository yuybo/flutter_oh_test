class AirPassengerInfo {
  //乘机人姓名
  String passengerName;
  //乘机人类型：0成人、1儿童或婴儿
  int passengerType;
  //会员卡号
  String? memberNo;
  //是否选中
  bool? isSelected = false;

  AirPassengerInfo({
    required this.passengerName,
    required this.passengerType,
    this.memberNo,
    this.isSelected,
  });

}