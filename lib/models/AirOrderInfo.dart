class AirOrderInfo {
  //机票价格
  double? ticketAmount;
  //民航基金
  double? airFund;
  //燃油费
  double? fuelAmount;
  //成人数
  int? personNum;
  //儿童数
  int? childNum;
  //婴儿数
  int? babyNum;

  AirOrderInfo({
    this.ticketAmount,
    this.airFund,
    this.fuelAmount,
    this.personNum,
    this.childNum,
    this.babyNum,
  });

  ///计算金额合计
  double getTotalAmount() {
    return ticketAmount!*personNum!.toDouble() + airFund!*personNum!.toDouble() + fuelAmount!*personNum!.toDouble();
  }
}