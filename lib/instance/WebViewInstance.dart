import 'package:webview_flutter/webview_flutter.dart';

class WebViewInstance {
  //静态初始化WebViewController
  static final WebViewController _controller = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.unrestricted);

  static WebViewController get controller => _controller;
}
