import 'dart:io';

import 'package:webview_flutter_ohos/webview_flutter_ohos.dart';
import 'package:webview_flutter_platform_interface/webview_flutter_platform_interface.dart';

abstract class AbstractWebViewController {
  static PlatformWebViewController controllerCache = PlatformWebViewController(
    OhosWebViewControllerCreationParams(),
  )..setJavaScriptMode(JavaScriptMode.unrestricted);

  PlatformWebViewController controller = controllerCache;

  PageEventCallback? onPageFinished;
  PageEventCallback? onPageStarted;
  ProgressCallback? onProgress;
  WebResourceErrorCallback? onWebResourceError;
  NavigationRequestCallback? onNavigationRequest;

  Future<void> loadFile(String absoluteFilePath) {
    return controller.loadFile(absoluteFilePath);
  }

  Future<void> loadFlutterAsset(String key) {
    return controller.loadFlutterAsset(key);
  }

  Future<void> loadHtmlString(
    String html, {
    String? baseUrl,
  }) {
    return controller.loadHtmlString(html, baseUrl: baseUrl);
  }

  Future<void> loadRequest(
    LoadRequestParams params,
  ) {
    return controller.loadRequest(params);
  }

  Future<bool> canGoBack() => controller.canGoBack();

  Future<bool> canGoForward() => controller.canGoForward();

  Future<void> goBack() => controller.goBack();

  Future<void> goForward() => controller.goForward();

  Future<void> reload() => controller.reload();

  Future<void> clearCache() => controller.clearCache();

  Future<String?> getTitle() {
    return controller.getTitle();
  }

  Future<void> runJavaScript(String javaScript) {
    return controller.runJavaScript(javaScript);
  }

  Future<void> setUserAgent(String? userAgent) {
    if (Platform.isIOS) {
      return Future.value();
    }
    return controller.setUserAgent(userAgent);
  }

  Future<void> setOnPlatformPermissionRequest(
    void Function(PlatformWebViewPermissionRequest request) onPermissionRequest,
  ) {
    return controller.setOnPlatformPermissionRequest(onPermissionRequest);
  }

  Future<String?> getUserAgent() {
    if (Platform.isIOS) {
      return Future.value("iOS");
    }
    return controller.getUserAgent();
  }

  Future<void> setOnConsoleMessage(
      void Function(JavaScriptConsoleMessage consoleMessage) onConsoleMessage) {
    if (Platform.isIOS) {
      return Future.value();
    }
    return controller.setOnConsoleMessage(onConsoleMessage);
  }

  Future<void> setOnJavaScriptTextInputDialog(
      Future<String> Function(JavaScriptTextInputDialogRequest request)
          onTextInputDialogRequest) {
    return controller.setOnJavaScriptTextInputDialog(onTextInputDialogRequest);
  }
}
