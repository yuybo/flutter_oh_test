import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../models/AirPassengerInfo.dart';

class SelectPassenger extends StatefulWidget {
  const SelectPassenger({super.key});

  @override
  State<SelectPassenger> createState() => _SelectPassengerState();
}

class _SelectPassengerState extends State<SelectPassenger> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('选择乘机人')),
      body: Center(
        child: buildPassengerWidget(),
      ),
    );
  }

  Widget buildPassengerWidget() {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const <Widget>[
                Icon(Icons.shopping_cart, color: Colors.yellow),
                Text("指该乘机人为当前账户受益人，请注意在“受益人\n管理”中维护所需的乘机证件",
                    softWrap: true,
                    textAlign: TextAlign.left,
                    maxLines: 2,
                    style: TextStyle(fontSize: 14, color: Colors.grey)),
              ],
            ),
          ),
          SizedBox(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const <Widget>[
                Icon(Icons.shopping_cart, color: Colors.yellow),
                Text("指该乘机人在第一程航班起飞时间为婴儿或儿童",
                    softWrap: true,
                    textAlign: TextAlign.left,
                    maxLines: 2,
                    overflow: TextOverflow.fade,
                    style: TextStyle(fontSize: 14, color: Colors.grey)),
              ],
            ),
          ),
          SizedBox(height: 200, child: buildPassengerListWidget())
        ],
      ),
    );
  }

  Widget buildPassengerListWidget() {
    return GridView.builder(
      padding: const EdgeInsets.all(5),
      itemCount: listData.length + 1,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio: 2.5,
      ),
      itemBuilder: _initGridViewData,
    );
  }

  Widget _initGridViewData(context, int index) {
    if (index == listData.length) {
      return DottedBorder(
        borderType: BorderType.RRect,
        child: InkWell(
          child: Container(
            alignment: Alignment.center,
            child: const Text("+新增乘机人",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14, height: 2.0, color: Colors.red)),
          ),
          onTap: () {
            print("新增乘机人");
          },
        ),
      );
    } else {
      return InkWell(
        onTap: () {
          setState(() {
            listData[index].isSelected = !listData[index].isSelected!;
          });
        },
        child: Container(
            color: const Color.fromARGB(237, 245, 245, 245),
            child: Stack(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: Text(listData[index].passengerName,
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontSize: 14, height: 2.5)),
                ),
                Visibility(
                  visible: listData[index].passengerType == 1,
                  child: const Positioned(
                    top: 0,
                    left: 0,
                    child: Icon(Icons.face, size: 15, color: Colors.blue),
                  ),
                ),
                Visibility(
                  visible: listData[index].isSelected!,
                  child: const Positioned(
                    top: 0,
                    right: 0,
                    child: Icon(Icons.done_outlined,
                        size: 15, color: Colors.red, fill: 1.0),
                  ),
                ),
              ],
            )),
      );
    }
  }

  List<AirPassengerInfo> listData = [
    AirPassengerInfo(
        passengerName: "我",
        passengerType: 0,
        memberNo: "123456789",
        isSelected: false),
    AirPassengerInfo(
        passengerName: "李四",
        passengerType: 0,
        memberNo: "123456789",
        isSelected: true),
    AirPassengerInfo(
        passengerName: "儿童",
        passengerType: 1,
        memberNo: "123456789",
        isSelected: false),
    AirPassengerInfo(
        passengerName: "赵六",
        passengerType: 0,
        memberNo: "123456789",
        isSelected: false)
  ];
}
