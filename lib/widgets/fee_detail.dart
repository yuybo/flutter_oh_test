import 'package:flutter/material.dart';
import '../models/AirOrderInfo.dart';

class FeeDetail extends StatefulWidget {

  final AirOrderInfo airOrderInfo;
  const FeeDetail({super.key, required this.airOrderInfo});

  @override
  State<FeeDetail> createState() => _FeeDetailState(airOrderInfo);
}

class _FeeDetailState extends State<FeeDetail>{

  //订单信息
  final AirOrderInfo airOrderInfo;

  _FeeDetailState(this.airOrderInfo);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("费用明细"),
      ),
      body: Center(
        child: buildFeeDetail(),
      ),
    );
  }

  Widget buildFeeDetail() {
    double totalAmount = airOrderInfo.getTotalAmount();
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const <Widget>[
              Text("机票：", style: TextStyle(fontSize: 14, height: 2.0)),
            ],
          ),
          //buildFeeDetailItem("机票：", 0),
          const Divider(height: 2.0, color: Colors.black),
          //费用明细
          buildFeeDataTable(airOrderInfo),
          //合计
          buildTotalItem("*项目为航司收取", totalAmount),

          buildFeeDetailItem("保险：", 0),
          const Divider(height: 2.0, color: Colors.black),
          buildFeeDetailItem("优惠券/码：", 0),
          const Divider(height: 2.0, color: Colors.black),
          //金额总计
          buildFeeDetailItem("金额总计：", totalAmount),
          buildBottomBar(totalAmount)
        ],
      ),
    );
  }

  ///构建费用明细
  Widget buildFeeDataTable(AirOrderInfo airOrderInfo) {
    return DataTable(
      headingRowHeight: 30,
      dataRowHeight: 30,
      horizontalMargin: 10,
      //columnSpacing: 20,
      columns: <DataColumn>[
        const DataColumn(
          label: Text('费用'),
        ),
        DataColumn(
          label: Text("成人x${airOrderInfo.personNum}"),
        ),
        DataColumn(
          label: Text("儿童x${airOrderInfo.childNum}"),
        ),
        DataColumn(
          label: Text("婴儿x${airOrderInfo.babyNum}"),
        ),
      ],
      rows: <DataRow>[
        DataRow(
          cells: <DataCell>[
            const DataCell(Text('机票')),
            DataCell(Text("${airOrderInfo.ticketAmount}x${airOrderInfo.personNum}")),
            const DataCell(Text('/')),
            const DataCell(Text('/')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            const DataCell(Text('民航基金')),
            DataCell(Text("${airOrderInfo.airFund}x${airOrderInfo.personNum}")),
            const DataCell(Text('/')),
            const DataCell(Text('/')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            const DataCell(Text('燃油费')),
            DataCell(Text("${airOrderInfo.fuelAmount}x${airOrderInfo.personNum}")),
            const DataCell(Text('/')),
            const DataCell(Text('/')),
          ],
        ),
        const DataRow(
          cells: <DataCell>[
            DataCell(Text('其他')),
            DataCell(Text('/')),
            DataCell(Text('/')),
            DataCell(Text('/')),
          ],
        )
      ]
    );
  }

  ///构建费用明细项
  Widget buildFeeDetailItem(String label, double amount) {
    String amountStr = amount > 0 ? "￥$amount" : "/";
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(label, style: const TextStyle(fontSize: 14, height: 2.0)),
        Text(amountStr)
      ],
    );
  }

  ///合计
  Widget buildTotalItem(String label, double amount) {
    String amountStr =  "合计：￥$amount";
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(label, style: const TextStyle(fontSize: 14, color: Color.fromARGB(255, 255, 106, 106), height: 2.0)),
        Text(amountStr)
      ],
    );
  }

  ///底部栏
  Widget buildBottomBar(double amount) {
    return Container(
      height: 50,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              Icon(Icons.shopping_cart, color: Colors.red),
              Text("积分", style: TextStyle(fontSize: 16, color: Colors.red)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text("合计："),
              Text("￥${amount.toString()}", style: const TextStyle(fontSize: 16, color: Colors.red)),
              const Icon(Icons.arrow_downward, color: Colors.red)
            ],
          ),
          ElevatedButton(
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("预定"),
          )
        ],
      ),
    );
  }

}