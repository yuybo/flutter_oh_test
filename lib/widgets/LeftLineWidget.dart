import 'package:flutter/material.dart';

class LeftLineWidget extends StatelessWidget {
  final bool showTop;
  final bool showBottom;
  //是否实线
  final bool isSolid;

  final double yStart;
  final double yEnd;

  const LeftLineWidget(this.showTop, this.showBottom, this.isSolid, this.yStart, this.yEnd, {super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      //margin: EdgeInsets.symmetric(horizontal: 16),
      width: 30,
      child: CustomPaint(
        painter: LeftLinePainter(showTop, showBottom, isSolid, yStart, yEnd)
      ),
    );
  }
}

class LeftLinePainter extends CustomPainter {
  static const double _topHeight = 15;

  final bool showTop;
  final bool showBottom;
  //是否实线
  final bool isSolid;

  final double yStart;
  final double yEnd;

  const LeftLinePainter(this.showTop, this.showBottom, this.isSolid, this.yStart, this.yEnd);

  @override
  void paint(Canvas canvas, Size size) {
    double lineWidth = 1;
    double centerX = size.width / 2;
    Paint linePain = Paint();
    linePain.color = Colors.red;
    linePain.strokeWidth = lineWidth;
    linePain.strokeCap = StrokeCap.square;
    if(!isSolid){
      linePain.style = PaintingStyle.stroke;
    }
    if(showTop){
      canvas.drawLine(Offset(centerX, yStart), Offset(centerX, yEnd), linePain);
    }
    /*if(showBottom){
      canvas.drawLine(Offset(centerX, 30), Offset(centerX, 45), linePain);
    }*/
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
