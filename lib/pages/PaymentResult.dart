import 'package:flutter/material.dart';

class PaymentResult extends StatelessWidget {
  final bool isSuccess;

  const PaymentResult({super.key, required this.isSuccess});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: isSuccess ? SuccessPage() : FailurePage(),
      ),
    );
  }
}

class SuccessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Text('支付成功',
        style: TextStyle(fontSize: 24, color: Colors.green));
  }
}

class FailurePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Text('支付失败',
        style: TextStyle(fontSize: 24, color: Colors.red));
  }
}
