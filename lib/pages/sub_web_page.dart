/*
* Copyright (c) 2023 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import 'package:flutter/material.dart';
import 'package:webview_flutter_platform_interface/webview_flutter_platform_interface.dart';

import '../instance/hna_webview_controller_impl.dart';

class SubWebPage extends StatefulWidget {
  const SubWebPage({super.key, required this.url});

  final String url;

  @override
  State<SubWebPage> createState() => _SubWebPageState();
}

class _SubWebPageState extends State<SubWebPage> {
  // 1
  late PlatformWebViewController controller;
  String message = "init";
  int paramId = 0;
  var contextRef;

  @override
  void initState() {
    super.initState();
    _initWebView();
  }

  _initWebView() async {
    // 2
    CustomWebViewControllerImpl hnaWebViewControllerImpl =
        CustomWebViewControllerImpl();
    controller = hnaWebViewControllerImpl.controller;
    //print(controller.hashCode);
    //print(controller);
    controller.addJavaScriptChannel(JavaScriptChannelParams(
      name: "JsObj",
      onMessageReceived: (JavaScriptMessage message) => {nextSubPage()},
    ));
    controller.setOnJavaScriptTextInputDialog(
        (JavaScriptTextInputDialogRequest request) async {
      return request.message;
    });
    //controller.loadFlutterAsset("html/index.html");
    controller
        .loadRequest(LoadRequestParams(uri: Uri.parse("https://m.hnair.com")));
    //controller.loadRequest(LoadRequestParams(uri: Uri.parse(widget.url)));
  }

  @override
  Widget build(BuildContext context) {
    contextRef = context;
    return Scaffold(
      //resizeToAvoidBottomInset: false,
      //appBar: AppBar(title: const Text('WebPage')),
      body: Column(children: [
        Expanded(
          child: PlatformWebViewWidget(
                  PlatformWebViewWidgetCreationParams(controller: controller))
              .build(context),
        ),
      ]),
    );
  }

  Future<void> nextSubPage() async {
    /* Navigator.push(
      contextRef,
      MaterialPageRoute(builder: (context) => const SubWebPageTwo()),
    );*/
    controller
        .loadRequest(LoadRequestParams(uri: Uri.parse("https://m.hnair.com")));
  }
}
