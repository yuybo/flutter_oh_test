/*
* Copyright (c) 2023 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import 'package:flutter/material.dart';
import 'package:webview_flutter_platform_interface/webview_flutter_platform_interface.dart';

import '../instance/hna_webview_controller_impl.dart';

class SubWebPageTwo extends StatefulWidget {
  const SubWebPageTwo({super.key});

  @override
  State<SubWebPageTwo> createState() => _SubWebPageTwoState();
}

class _SubWebPageTwoState extends State<SubWebPageTwo> {
  // 1
  late PlatformWebViewController controller;
  String message = "init";
  int paramId = 0;

  @override
  void initState() {
    super.initState();
    _initWebView();
  }

  _initWebView() async {
    // 2
    CustomWebViewControllerImpl hnaWebViewControllerImpl =
        CustomWebViewControllerImpl();
    controller = hnaWebViewControllerImpl.controller;
    print(controller.hashCode);
    print(controller);
    /*controller.addJavaScriptChannel(
      "Toaster",
      onMessageReceived: (JavaScriptMessage message) => {
        setState(() {
          this.message = message.message;
        })
      },
    );*/
    controller.loadFlutterAsset("html/index1.html");
    //controller.loadRequest(LoadRequestParams(uri: Uri.parse("https://m.hnair.com")));
    //controller.loadRequest(LoadRequestParams(uri: Uri.parse(widget.url)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(title: const Text('WebPage')),
      body: PlatformWebViewWidget(
              PlatformWebViewWidgetCreationParams(controller: controller))
          .build(context),
    );
  }
}
